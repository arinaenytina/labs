#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <cmath>
#include <vector>
using namespace std;

int from_left_to_right(unsigned long long x, unsigned long long y){
    if (y == 0) return 1;
    if (x == 0) return 0;
    unsigned long long z = x;
    long t = 2;
    long n = 0;
    for(; y >= t;  n++) 
        t *= 2;
    n++;
    for(int i = n - 2; i >= 0; i--){
        z = (z * z);
        if (((y >> i) & 1) == 1) 
            z = (z * x);
    }
    return z;      
}


unsigned long long Extracting_the_root(unsigned long long a, unsigned long long n){
    unsigned long long a0 = a;
    unsigned long long a1 = ((a / (from_left_to_right(a0, n - 1))) + (n - 1) * a0) / n;
    while (a0 > a1){
        a0 = a1;
        a1 = ((a/(from_left_to_right(a0, n - 1))) + (n - 1) * a0) / n;
    }
    return a0;

}

vector<int> remove_duplicate(vector<int>Q){
    
    sort(Q.begin(), Q.end());
    auto last = unique(Q.begin(), Q.end());
    Q.erase(last,Q.end());
    
    return Q;

}

bool belongs(int x, vector<int>Q){
    for(int i = 0; i<Q.size();i++){
        if (x == Q[i])
            return true;
    }
    return false;
}

string ferma (int n){
    string res = "";
    int x = Extracting_the_root(n, 2);
    if (x*x == n && x!= 1){
        res += to_string(x) + " " + to_string(x);
        return res;
    }

    
    int M[] = {3, 4, 5};

    //квадратичные вычиты
    vector<int> tmp;
    vector<int>Q[3];
    for(int k = 0; k<3; k++){
        for(int i = 0;i < M[k]; i++){
            int t = i * i % M[k];
            if (t!= 0)
                tmp.push_back(t);
    }
    Q[k] = remove_duplicate(tmp);
    }

    //таблица просеивания
    int s[3][5];
    int t = 0;
    for(int i = 0; i < 3; i++){
        for (int j = 0; j < 5; j++){
                t = (j * j - n) % M[i]; 
                if (t < 0)
                    t += M[i];
                if ((t == 0) || belongs(t, Q[i]))
                    s[i][j] = 1;
            else
                s[i][j] = 0;
        }
    }
   
    x++; 
    
    int r[3];
    for (int i = 0; i<3;i++)
        r[i] = x%M[i];
 
    // //просеивание
    int c = 0;
    int z = 0;
    int y = 0;
    int B = (n+9)/6;
   
    while(x <= B){
        c = 0;
        for (int k = 0; k<3;k++){
            if (s[k][r[k]] == 1)
                c++;
            if(c == 3){
                z = (x * x) - n;
               
                y = Extracting_the_root(z,2);
                if((y * y) == z && (x-y) != 1) {
                    res += to_string(x+y) + " "+ to_string(x-y);
                    return res;
                }
            }
        }
        x++;
        for(int i = 0; i < 3; i++)
            r[i] = (r[i] + 1) % M[i];
        
    }
return "";

}

int main(){
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()){
        return 0;}
    unsigned long long mas[1];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 1; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' ' && flag == false){
            index++;
            if(index > 0){
                flag = true;
            }
        } else if(tmp == '\n'){ 
            if(!flag && index == 0 && mas[0] > 0){
                int n = mas[0];
                string result = "";
                while((n & 1) == 0){//пока число чётное, делить на 2
		            n = n >> 1;
                    }
                result = ferma(n);
                if(result != "") out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 1; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1 && flag ==false)
                mas[index] = mas[index] * 10 + (tmp - '0');
                
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}
