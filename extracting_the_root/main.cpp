#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>

using namespace std;

int from_left_to_right(unsigned long long x, unsigned long long y){
    if (y == 0) return 1;
    if (x == 0) return 0;
    unsigned long long z = x;
    long t = 2;
    long n = 0;
    for(; y >= t;  n++) 
        t *= 2;
    n++;
    for(int i = n - 2; i >= 0; i--){
        z = (z * z);
        if (((y >> i) & 1) == 1) 
            z = (z * x);
    }
    return z;      
}


unsigned long long Extracting_the_root(unsigned long long a, unsigned long long n){
    unsigned long long a0 = a;
    unsigned long long a1 = ((a / (from_left_to_right(a0, n - 1))) + (n - 1) * a0) / n;
    while (a0 > a1){
        a0 = a1;
        a1 = ((a/(from_left_to_right(a0, n - 1))) + (n - 1) * a0) / n;
    }
    cout<<a0<<endl;
    return a0;

}



int main(){
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()) 
        return 0;
    unsigned long long mas[2];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 2; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' '){
            index++;
            if(index > 1){
                flag = true;
            }

        } else if(tmp == '\n'){ 
            if(!flag && index == 1){
                unsigned long long result = Extracting_the_root(mas[0], mas[1]);
                out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 2; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1)
                mas[index] = mas[index] * 10 + (tmp - '0');
            
        }
        
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}
