
#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <fstream>
using namespace std;

vector<int> create_base(int k){//создание факторной базы первые к простых
    vector<int>S;
    S.push_back(2);
    S.push_back(3);
    int n = 5;
    bool f = true;
   
    while(S.size() != k){
        for(int j=0; j<S.size(); j++){
            if((n % S[j]) == 0)
                f = false; 
        }
        if(f){
        S.push_back(n); 
        }
        n+=2; f = true;
    }
    return S;
}

int from_left_to_right(unsigned long long x, unsigned long long y, unsigned long long m){//x**y mod m
    if (y == 0) return 1;
    if (x == 0) return 0;
    unsigned long long z = x;
    long t = 2;
    long n = 0;
    for(; y >= t;  n++) 
        t *= 2;
    n++;
    for(int i = n - 2; i >= 0; i--){
        z = (z * z) % m;
        if (((y >> i) & 1) == 1) 
            z = (z * x) % m;
    }
    return z;      
}

int find_gen(int n){//поиск порождающего
    int p = n - 1;
    int s = sqrt(p);
    vector<int> primes = create_base(s);

    vector<int> divisors;

    for(int i = 0; i<primes.size(); i++){
        if(p % primes[i] == 0)
            divisors.push_back(primes[i]);
    }

    for(int i = 2; i < n; i++){
        bool isGenerator = true;
        for(int d = 0; d<divisors.size(); d++){
            int k = p / divisors[d];
            if (from_left_to_right(i, k, n) == 1){
                isGenerator = false;
                break;
            }
        }
        if(isGenerator) return i;
    }
}


map<int,int> deviders(int n){
    int s = sqrt(n);
    vector<int> p = create_base(s+1);
    map<int,int> d;
    for(int i = 0; i < p.size(); i++){
        while(n % p[i] == 0){
            d[p[i]]++;
            n /= p[i];
        }
    }
    return d;
}

int phi (int n) {//функция эйлера
    int result = n;
    for (int i=2; i*i<=n; ++i)
        if (n % i == 0) {
            while (n % i == 0)
                n /= i;
            result -= result / i;
        }
    if (n > 1)
        result -= result / n;
    return result;
}

int inv_pow(int g, int y, int n){//g**-y mod n
    while(y < 0){
        y += phi(n);
    }
    int k = 1;
    for(int l = 1; l<= y; l++){
        k = k * g % n;
    }
    return k;
}

int pollig_hellman(int n, int a){
    int g = find_gen(n);//поиск порождающего
    int p = n-1;
    map <int,int> factors = deviders(p);//разложение на множители
    auto size = factors.size();//число делителей
    vector<map<int,int>>r;
    vector<int> x;
    int alpha;
    int p_i;
    //построение таблиц
    for(auto it = factors.begin(); it != factors.end(); it++){
        p_i = it->first;
        alpha = from_left_to_right(g, p/p_i, n);
        map<int, int>r_i;
        for(int j = 0; j<p_i; j++){
            int r_j = from_left_to_right(alpha,j,n);
            r_i[r_j] = j;
        }
        r.push_back(r_i);
    }
    //вычисление х
    int i = 0;
    map<int, int>::iterator itr;
    for(auto it = factors.begin(); it != factors.end(); it++){
        p_i = it->first;
        int e = it->second;
        //х0
        int b = from_left_to_right(a,n/p_i, n);
        map<int, int> r_i = r[i];
		itr = r_i.find(b);
		int y = itr->second;
        //2.2
        for(int j = 1; j < e; j++){
           // cout<<"a = "<<a<<" g = "<<g<<" y = "<<y<<" p_i = "<<p_i<<" p = "<<p<<" j+1 = "<<j+1<<endl;
            int pp = inv_pow(g,y,n);
            b = from_left_to_right(a * inv_pow(g,-y,n), p / (from_left_to_right(p_i, j+1, n)), n);
            itr = r_i.find(b);
            int x_j = itr->second;
            y += x_j * from_left_to_right(p_i, j, n);
        }
        x.push_back(y);
        i++;
    }
    int result = 0;
    //по КТО x mod n

    vector<int> vec;
	int q;
	for (auto it = factors.begin(); it != factors.end(); it++)
	{
		vec.push_back(pow(it->first, it->second));
	}
	
	for (int i = 0; i < x.size(); i++)
	{
		int M_i = p / vec[i];
		q = inv_pow(M_i, -1, vec[i]);
		result += x[i] *M_i * q;
		
	}
	if (result > p) {
		result %= p;
	}
    return result;
}

bool IsValid(int n, int a){
    if (a >= n)
        return false;

    //isPrime
    bool isPrime = false;
    unsigned long i;
    if (n == 2)
        return true;
    if (n == 0 || n == 1 || n % 2 == 0)
        return false;
    for(i = 3; i*i <= n && n % i; i += 2)
        ;
    if (i*i > n)
        isPrime = true;
    if (isPrime) return true;
    return false;

}


int main(){
   
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()){
        return 0;}
    unsigned long long mas[2];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 2; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' ' && flag == false){
            index++;
            if(index > 1){
                flag = true;
            }
        } else if(tmp == '\n'){ 
            if(!flag && index == 1 && IsValid(mas[0], mas[1])){
                int result = pollig_hellman(mas[0], mas[1]);
                    out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 2; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1 && flag ==false)
                mas[index] = mas[index] * 10 + (tmp - '0');
                
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}

