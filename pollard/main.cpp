
#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <fstream>

using namespace std;

int from_left_to_right(unsigned long long x, unsigned long long y, unsigned long long m){//x**y mod m
    if (y == 0) return 1;
    if (x == 0) return 0;
    unsigned long long z = x;
    long t = 2;
    long n = 0;
    for(; y >= t;  n++) 
        t *= 2;
    n++;
    for(int i = n - 2; i >= 0; i--){
        z = (z * z) % m;
        if (((y >> i) & 1) == 1) 
            z = (z * x) % m;
    }
    return z;      
}

vector<int> create_base(int k){//создание факторной базы первые к простых
    vector<int>S;
    S.push_back(2);
    S.push_back(3);
    int n = 5;
    bool f = true;
   
    while(S.size() != k){
        for(int j=0; j<S.size(); j++){
            if((n % S[j]) == 0)
                f = false; 
        }
        if(f){
        S.push_back(n); 
        }
        n+=2; f = true;
    }
    return S;
}

int find_gen(int n){//поиск порождающего
    int p = n - 1;
    int s = sqrt(p);
    vector<int> primes = create_base(s);

    vector<int> divisors;

    for(int i = 0; i<primes.size(); i++){
        if(p % primes[i] == 0)
            divisors.push_back(primes[i]);
    }

    for(int i = 2; i < n; i++){
        bool isGenerator = true;
        for(int d = 0; d<divisors.size(); d++){
            int k = p / divisors[d];
            if (from_left_to_right(i, k, n) == 1){
                isGenerator = false;
                break;
            }
        }
        if(isGenerator) return i;
    }
}

int gcd(int a, int b) {
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

vector <int> F(vector <int> i, int a, int g, int n) {
    int x = i[0];
    int y = i[1];
    int z = i[2];
    if ((x % 3) == 1){
        x = (a * x) % n;
        y = (y + 1) % (n - 1);
        //z = z 

    }

    else if (x % 3 == 2){
        x = (x * x) % n;
        y = (2 * y) % (n - 1);
        z = (2 * z) % (n - 1);
    }

    else if (x % 3 == 0){
        x = (g * x) % n;
        //y=y
        z = (z + 1) % (n - 1);
    }
    vector<int> res(3);
    res[0] = x;
    res[1] = y;
    res[2] = z;
    return res;
}

int pollard(int n, int a){

    int g = find_gen(n);
   // cout<<"g = "<<g<<endl;
    int x_i = 1, x_2i = 1;
    int y_i = 0, y_2i = 0;
    int z_i = 0, z_2i = 0;

    vector <int> i(3);
    i[0] = x_i;
    i[1] = y_i;
    i[2] = z_i;

    vector <int> i_2(3);
    i_2[0] = x_2i;
    i_2[1] = y_2i;
    i_2[2] = z_2i;

    do {
        i = F(i, a, g, n);
        i_2 = F(F(i_2, a, g, n),a,g,n);
    }  while(i[0] != i_2[0]);

        x_i = i[0];
        x_2i = i_2[0];

        y_i = i[1];
        y_2i = i_2[1];

        z_i = i[2];
        z_2i = i_2[2];

    if(y_i == y_2i) return -1;

    int d = gcd(n - 1 + y_i - y_2i, n - 1);

    vector <int> b;
    for (int j = 1; j < n; j++){
        int y = ((n-1 + y_i - y_2i) * j) % (n - 1);

        int z = (n-1 + z_2i - z_i) % (n - 1);

        if (y == z)
            b.push_back(j);
    }

    for (int j = 0; j < d; j++){
        if (from_left_to_right(g, b[j], n) == a)//g**b mod n
            return b[j];
    }
    return -1;
}

bool IsValid(int n, int a){
    if (a >= n)
        return false;

    //isPrime
    bool isPrime = false;
    unsigned long i;
    if (n == 2)
        return true;
    if (n == 0 || n == 1 || n % 2 == 0)
        return false;
    for(i = 3; i*i <= n && n % i; i += 2)
        ;
    if (i*i > n)
        isPrime = true;
    return isPrime;

}


int main(){
   
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()){
        return 0;}
    unsigned long long mas[2];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 2; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' ' && flag == false){
            index++;
            if(index > 1){
                flag = true;
            }
        } else if(tmp == '\n'){ 
            if(!flag && index == 1 && IsValid(mas[0], mas[1])){
                int result = pollard(mas[0], mas[1]);
                if (result != -1)
                    out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 2; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1 && flag ==false)
                mas[index] = mas[index] * 10 + (tmp - '0');
                
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}

