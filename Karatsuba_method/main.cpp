#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
using namespace std;

unsigned long long Karatsuba_method(unsigned long long u, unsigned long long v){
    unsigned long long max_num = v;
    if (u > v) max_num = u;
    unsigned long long bits = 0;
    while (max_num!=0)
    {
        max_num = max_num >> 1;
        bits++;
    }
   
    
 
    unsigned long long n = (bits%2 ==0) ? (bits/2) : (bits/2 +1);
    unsigned long long u0, u1, v0, v1;
    u1 = u >> n;
    v1 = v >> n;
    u0 =((1 << n) - 1) & u;
    v0 =((1 << n) - 1) & v;    
    unsigned long long C = (u1 + u0)*(v1+v0);
    unsigned long long A = u1*v1;
    unsigned long long B = u0*v0;
    unsigned long long res = (A << (2*n)) + ((C-A-B)<<n) + B;
    //cout<<"u = "<<u0<<" "<<u1<<endl;
   // cout<<"v = "<<u0<<" "<<u1<<endl;
    return res;
}


int main(){
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()) 
        return 0;
    unsigned long long mas[2];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 2; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' '){
            index++;
            if(index > 1){
                flag = true;
            }

        } else if(tmp == '\n'){ 
            if(!flag && index == 1){
                unsigned long long result = Karatsuba_method(mas[0], mas[1]);
                out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 2; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1)
                mas[index] = mas[index] * 10 + (tmp - '0');
            
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}
