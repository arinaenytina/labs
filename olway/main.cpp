#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cmath>
using namespace std;

int from_left_to_right(unsigned long long x, unsigned long long y){
    if (y == 0) return 1;
    if (x == 0) return 0;
    unsigned long long z = x;
    long t = 2;
    long n = 0;
    for(; y >= t;  n++) 
        t *= 2;
    n++;
    for(int i = n - 2; i >= 0; i--){
        z = (z * z);
        if (((y >> i) & 1) == 1) 
            z = (z * x);
    }
    return z;      
}


unsigned long long Extracting_the_root(unsigned long long a, unsigned long long n){
    unsigned long long a0 = a;
    unsigned long long a1 = ((a / (from_left_to_right(a0, n - 1))) + (n - 1) * a0) / n;
    while (a0 > a1){
        a0 = a1;
        a1 = ((a/(from_left_to_right(a0, n - 1))) + (n - 1) * a0) / n;
    }
    return a0;

}



string olway (int n){
   
    int cbrt_n = Extracting_the_root(n, 3);
    //cout<<"кубич корень из n "<<cbrt_n<<endl;
    int d = 2 * cbrt_n + 1; 
    
    int r1 = n % d;
    int r2 = n % (d - 2);
    int q = 4*( floor(n / (d - 2)) - floor(n / d));
    int s = Extracting_the_root(n, 2);

    int r = 2 * r1 - r2 + q;
    while(true){
        d += 2;
        r = 2 * r1 - r2 + q;
        if (d > s)    
            {return "";}
        if (r < 0) 
            {r += d; q += 4;}
        while (r >= d) 
            {r -= d; q -= 4;}
        if (r == 0) 
            return to_string(d);
        else {r2 = r1; r1 = r;}
    }
}

int main(){
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()){
        return 0;}
    unsigned long long mas[1];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 1; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' ' && flag == false){
            index++;
            if(index > 0){
                flag = true;
            }
        } else if(tmp == '\n'){ 
            if(!flag && index == 0 && mas[0] > 0){
                int n = mas[0];
                int even_dividers = 0;
                string result = "";
                while((n & 1) == 0){//пока число чётное, делить на 2
		            n = n >> 1;
                    even_dividers +=2;
                    }
                result = olway(n);
              //  if (result == "" && even_dividers >= 2)
             //       result += to_string(even_dividers);//если у числа не нашлись делители, но были убраны двойки, возвращаю их
                if (result != "")
                    out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 1; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1 && flag ==false)
                mas[index] = mas[index] * 10 + (tmp - '0');
                
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}
