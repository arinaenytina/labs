#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>

using namespace std;

unsigned long long Barrett_method(unsigned long long x, unsigned long long m){
    if (x == m) return 0;
    int bits=0;
    unsigned long long tmp = m;
    while (tmp!=0)
    {
        tmp = tmp >> 1;
        bits++;
    }
    unsigned long long k = (bits%2 ==0) ? (bits/2) : (bits/2 +1);
    
    unsigned long long r = 0;
    unsigned long long z = (1<<(4*k)) / m;
    unsigned long long q = ((x >> 2*(k-1)) * z) >> 2*(k+1);
    unsigned long long r1 = x & ((1u<<(2*(k+1)))-1);
    unsigned long long r2 = (q*m) & ((1u<<(2*(k+1)))-1);
    if (r1>=r2){
        r = r1-r2;
    }

    else 
        r =(1<<2*(k+1)) + r1 - r2;
    while (r>=m){
        r = r-m;
    }
    return r;

}

bool check (unsigned long long x, unsigned long long m){
    int a = 0;
    while (m!=0)
    {
        m = m >> 1;
        a++;
    }
    unsigned long long k = (a%2 ==0) ? (a/2) : (a/2 +1);

    int b = 0;
    while (x!=0)
    {
        x = x >> 1;
        b++;
    }
    unsigned long long n = (b%2 ==0) ? (b/2) : (b/2 +1);
    if (n > 2*k) return false;
    return true;
}

int main(){
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()) 
        return 0;
    unsigned long long mas[2];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 2; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' '){
            index++;
            if(index > 1){
                flag = true;
            }

        } else if(tmp == '\n'){ 
            if(!flag && index == 1 && check(mas[0], mas[1])){
                unsigned long long result = Barrett_method(mas[0], mas[1]);
                out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 2; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1)
                mas[index] = mas[index] * 10 + (tmp - '0');
            
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();

    return 1;
}