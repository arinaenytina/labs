#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <vector>
using namespace std;



string trial_division(int n, int b){
    int r = 0, q = 0;
    int d = 3;
    string res = "";
    int num = n;
    //bool flag = true;
    while (d <= b && n!=1){
        r = n % d;
        q = n / d;

        if (r == 0){
            res += to_string(d) + " ";
            n = q;
        } else if (d < q)
            d+=2;
        else {
            res += to_string(n) + " ";
            n = 1;
        }
    }
    res += to_string(n);
    if (n > 1)
        res += " 1";
    else   res += " 0";
    return res; 
}


int main(){
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()){
        return 0;}
    unsigned long long mas[2];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 2; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' '){
            index++;
            if(index > 1){
                flag = true;
            }

        } else if(tmp == '\n'){ 
            if(!flag && index == 1){
                int n = mas[0];
                string result = "";
                while((n & 1)==0){//пока число чётное, делить на 2
		            result += "2 ";
		            n = n>>1;}
                result += trial_division(n, mas[1]);
                out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 2; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1)
                mas[index] = mas[index] * 10 + (tmp - '0');
            
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();

    return 1;
}
