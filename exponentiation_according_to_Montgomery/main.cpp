#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>

using namespace std;

void check(unsigned long long a) {
    int bits = 0;
    while (a != 0) {
        bits += (a & 1u);
        a >>= 1u;
    }
    if (bits != 1) 
        throw exception();

}

unsigned long long transformation(unsigned long long x, unsigned long long b, unsigned long long m) {
    unsigned long long z = x;
    unsigned long long u = 0;
    int p, k = 0;

    for (p = 0; ((unsigned long long)b & ((unsigned long long)1 << p)) == 0; p++);
    while ((m >> p * k) != 0) k++;

    if (x > (m * ((unsigned long long)1 << (k *p))))
        throw exception();

    long long m1 = 1;
    while ((m1 * (m & (b-1))) % b != 1) m1++;//m0(-1)
    m1 = b - m1;//-m'

    for (int i = 0; i < k; i++) {
        u = (((z >> (p * i)) & (b - 1)) * m1) % b;
        z += u * m * ((unsigned long long)1 << (i *p));
    }
    z /= ((unsigned long long)1 << (k * p));
    if (z >= m)
        z -= m;
    return z;
}
unsigned long long multiplication(unsigned long long x, unsigned long long y, unsigned long long b, unsigned long long m) {
    if (x > m || y > m) 
        throw exception();
    
    int p, k = 0;

    for (p = 0; ((unsigned long long)b & ((unsigned long long)1 << p)) == 0; p++);
    while ((m >> p * k) != 0) k++;

    long long m1 = 1;
    while ((m1 * (m & (b - 1))) % b != 1) m1++;
    m1 = b - m1;

    unsigned long long u, z = 0;

    for (int i = 0; i < k; i++) {
        u = (((z & (b - 1)) + ((x >> (p * i)) & (b - 1)) * (y & (b - 1))) * m1) % b;
        z = (z + ((x >> (p * i)) & (b - 1)) * y + u * m) / b;
    }
    if (z > m)
        z -= m;
    return z;
}
unsigned long long exponentiation(unsigned long long x, unsigned long long y, unsigned long long b, unsigned long long m) { 
    int p;
    int k = 0;
    int n = 0;
    for (p = 0; ((unsigned long long)b & ((unsigned long long)1 << p)) == 0; p++);
    while ((m >> p * k) != 0) k++;
    
    long long m1 = 1;
    while ((m1 * (m & (b - 1))) % b != 1) m1++;
    m1 = b - m1;
   
    int r2 = ((unsigned long long)1 << (2 * k * p)) % m;
    unsigned long long x1 = multiplication(x, r2, b, m);
    unsigned long long z = x1;
    while ((y >> n) != 0) n++;
   
 
    for (int i = n - 2; i >= 0; i--) {
     z = multiplication(z, z, b, m);
     if (((y >> i) & 1) == 1)
         z = multiplication(z, x1, b, m);
    }
    z = transformation(z, b, m);
    return z;
   
}

int main() { 
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt");
    if (!in.is_open() || !out.is_open())
        return 0;
    unsigned long long mas[4];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for (int i = 0; i < 4; i++)
        mas[i] = 0;
    while (!in.eof()) {
        if ((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n')
            flag = true;

        if (tmp == ' ') {
            index++;
            if (index >= 4) {
                flag = true;
            }

        }
        else if (tmp == '\n') {
            if (!flag &&  mas[0] < mas[2]) {
                try 
                {
                    check(mas[3]);
                    unsigned long long result = exponentiation(mas[0], mas[1], mas[3], mas[2]);
                   // cout << result;
                    out << result << endl;
                }
                catch(exception &e){}
            }

            flag = false;
            for (int i = 0; i < 4; i++)
                mas[i] = 0;
            index = 0;
        }
        else {
            if (index < 4)
                mas[index] = mas[index] * 10 + (tmp - '0');
            
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}
