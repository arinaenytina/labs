#include <iostream>
#include <fstream>
#include <string>

using namespace std;
//
int from_right_to_left(unsigned long long x, unsigned long long y, unsigned long long m){
    unsigned long long q = x;
    unsigned long long z;
    if((y&1) == 0)
        z = 1;
    else z = x;
    int t = 2;
    int n = 0;
    for(; y >= t;  n++) 
        t *= 2;
    n++;
    for(int i = 1; i < n; i++){
        q = (q * q) % m;
        if ((y >> i) & 1)
            z = (z * q) % m;
    }

    return z;      
}

int main (){
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open())
        return 0;
    unsigned long long mas[3];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 3; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' '){
            index++;
            if(index > 2){
                flag = true;
            }

        } else if(tmp == '\n'){ 
            if(!flag && mas[2] != 0){
                unsigned long long result = from_right_to_left(mas[0], mas[1], mas[2]);
                out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 3; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 2)
                mas[index] = mas[index] * 10 + (tmp - '0');
            
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}