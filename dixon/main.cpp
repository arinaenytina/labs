#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
using namespace std;

vector<int> create_base(int k){//создание факторной базы первые к простых
    vector<int>S;
    S.push_back(2);
    S.push_back(3);
    int n = 5;
    bool f = true;
   
    while(S.size() != k){
        for(int j=0; j<S.size(); j++){
            if((n % S[j]) == 0)
                f = false; 
        }
        if(f){
        S.push_back(n); 
        }
        n+=2; f = true;
    }
    return S;
}

vector<vector<int>> all_solutions(vector<vector<int>>V, int k, int t){
    vector<vector<int>>res;

    for(int i = 1; i < (1<<t); i++){
        vector<int>tmp(t,0);

        for(int j = 0;j<t;j++){//очередной набор
            if (i & (1 << j))
                tmp[j] = 1;
        }

        vector<int> mult(k,0);
        for(int j = 0; j < t; j++){
            if (tmp[j] == 1){
                for(int x = 0; x < k; x++)
                    mult[x] += (V[j])[x];//результат усножения вектора на матрицу
            }
        }

        bool f = true;
        for(int j = 0; j < k; j++){
            if(mult[j] % 2 != 0)
                f = false;
        }
        if(f)
            res.push_back(tmp);
    }



return res;
}

int gcd(int a, int b){
    if (b == 0)
        return a;
    return gcd(b, a % b);

}

int dixon(int n, int k){
    vector<int>S = create_base(k);
    int t = k + 1;
    vector<vector<int>>e(t, vector<int>(k,0));
    
    vector<int>a(t,0);
    vector<int>b(t,0);
   // 
    
    for(int i = 0; i < t; i++){
        int tmpb;
        do{
            a[i] = rand() % n;
            b[i] = (a[i] * a[i]) % n;
            for(int j = 0; j < k; j++){
                e[i][j] = 0;
            }
            tmpb = b[i];
            if(b[i] != 0){
                
                for(int j = 0; j < k; j++){
                    while(tmpb % S[j] == 0){
                        e[i][j]++;
                       tmpb /= S[j];
                    }
                }
            }
            else tmpb = 1000; //другое b
        
        } while (tmpb != 1);//1
    }

p:  
    vector<vector<int>>V(t, vector<int>(k,0));
    
    for(int i = 0; i < t;i++){
        for(int j = 0; j < k;j++){
            V[i][j] = e[i][j] % 2;  
            }
        
    }

    vector<vector<int>>res = all_solutions(V, k, t);

    for(int i = 0; i < res.size(); i++){//перебор решений для матрицы
        int x = 1;
        int y = 1;
        for(int j = 0; j < t; j++){//вычисление х, у
            if(res[i][j])
                x = (x * a[j]) % n;
        }

        for(int j = 0; j < k; j++){
            int p = 0;
            for(int r = 0; r < t;r++){
                if(res[i][r] == 1){
                    p += e[r][j];
                }
            }
            p /= 2;
            y *= (int)pow(S[j], p);
        }
        if((x % n) != (y % n) && (x+y) % n != 0)
            return gcd(x + y, n);

    }

    t++; //все решения перебрали строим еще пары}
    a.push_back(0);
    b.push_back(0);
    e.emplace_back(k);
    int s = a.size() - 1;

    do{
        a[s] = rand() % n;
        b[s] = (a[s] * a[s]) % n;


        for(int j = 0; j < k; j++){
            e[s][j] = 0;
        }


        if(b[s] != 0){
            
            for(int j = 0; j < k; j++){
                while(b[s] % S[j] == 0){
                    e[s][j]++;
                    b[s] /= S[j];
                }
            }
        }
        else b[s] = 1000; 
    
    } while (b[s] != 1);//1
    goto p;//добавление к матрице строки и поиск еще решений

}



bool IsValid(int n, int k){
    if (k < 2 || k >= sqrt(n))
        return false;

    //isPrime
    unsigned long i;
    if (n == 2)
        return true;
    if (n == 0 || n == 1 || n % 2 == 0)
        return false;
    for(i = 3; i*i <= n && n % i; i += 2)
        ;
    if (i*i > n)
        return false;
    return true;

}

int main(){
   
    ifstream in;
    ofstream out;
    out.open("output.txt");
    in.open("input.txt"); 
    if (!in.is_open() || !out.is_open()){
        return 0;}
    unsigned long long mas[2];
    int index = 0;
    char tmp;
    in.read(&tmp, 1);
    bool flag = false;
    for(int i = 0; i < 2; i++) 
        mas[i] = 0;
    while(!in.eof()){
        if((tmp < '0' || tmp > '9') && tmp != ' ' && tmp != '\n') 
            flag = true;
        
        if(tmp == ' ' && flag == false){
            index++;
            if(index > 1){
                flag = true;
            }
        } else if(tmp == '\n'){ 
            if(!flag && index == 1 && IsValid(mas[0], mas[1])){
                while((mas[0] & 1) == 0){//пока число чётное, делить на 2
		            mas[0] = mas[0] >> 1;
                    }
                    int result = dixon(mas[0], mas[1]);
                    out << result << endl;
            }

            flag = false;
            for(int i = 0; i < 2; i++) 
                mas[i] = 0;
            index = 0;
        } else {
            if(index <= 1 && flag ==false)
                mas[index] = mas[index] * 10 + (tmp - '0');
                
        }
        in.read(&tmp, 1);
    }
    out.close();
    in.close();
    return 1;
}